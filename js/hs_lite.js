(function ($) {
  Drupal.behaviors.hs_lite = {
    attach: function (context, settings) {
      /**
       * filterByData
       *
       * Searchs through data properties, and returns
       * the dom object that has the property and value
       * that was passed in.
       */
      $.fn.filterByData = function(prop, val) {
        return this.filter(
          function() { return $(this).data(prop)==val; }
        );
      }
      // Add a 'None' option if the select has been marked as required.
      // Our process of preparing the fields and marking their levels
      // assumes we have 'None' as the first option. The required select field
      // doesn't have this, so we need to add one.
      $('.hs-lite-widget.required').prepend('<option value="_none">- None -</option>');
      // Loop through the widgets, finding any marked as
      // a hs-lite widget.
      $('.hs-lite-widget').once(function() {
        new Drupal.hs_lite.widget($(this));
        $(this).hide();
      });
    }
  };

  Drupal.hs_lite = Drupal.hs_lite || {};

  Drupal.settings.hs_lite = Drupal.settings.hs_lite || {};

  Drupal.hs_lite.widget = function($field) {
    // Properties
    this.field = $field;
    this.id = $field.attr('id');
    this.elWidget;
    this.elSelects;
    this.elButton;
    this.elDropArea;
    this.optgroup = ($field.find('optgroup').length) ? true : false;
    // Initialize the widget
    this.prepareField();
    this.addWidget();
  }

  /**
   * Prepares the select field with data attributes.
   */
  Drupal.hs_lite.widget.prototype.prepareField = function() {
    var widget = this;
    var parents = [];
    var oldLevel = 1;

    // Loop through the options.
    widget.field.find("option, optgroup").each(function(index, value) {

      var $option = $(value);

      // Get the level of the current option
      var currentLevel = widget.getOptionLevel($option);

      // Skip -none-.
      if (currentLevel) {

        // If the old level isn't smaller then pop off the unneeded parents
        if (oldLevel >= currentLevel) {
          var offset = oldLevel - currentLevel + 1;
          for (i = 1; i <= offset; i++) {
            parents.pop();
          }
        }

        // If it's the first level the parent will be 0 otherwise it's the
        // last parent in the tracker array.
        var parent = (currentLevel == 1) ? 0 : parents[parents.length - 1];

        // Set data attributes.
        $option.data('id', index);
        $option.data('parent', parent);
        $option.data('level', currentLevel);

        // Add the current index to the tracker array.
        parents.push(index);

        // Now the oldLevel is the currentLevel
        oldLevel = currentLevel;

      }

    });

  } // end prepareField

  /**
   * Adds the widget to the field.
   */
  Drupal.hs_lite.widget.prototype.addWidget = function() {
    var widget = this;
    var parents = [];

    widget.elDropArea = $(
      '<div class="hs-lite-droparea">' +
        '<table style="width:auto">' +
        '<caption class="droparea-title">' + Drupal.t('All selections') + '</caption>' +
        '<tbody>' +
        '</tbody>' +
        '</table>' +
      '</div>');

    var empty = true;
    $(widget.field.find('option:selected').get().reverse()).each(function() {
      $option = $(this);
      if ((parents.indexOf($option.val()) === -1)) {
        var selectedIds = [];
        var selectedNames = [];
        var $checkedOption = $option;
        if ($checkedOption.data('level') != undefined) {
          while ((!widget.optgroup && $checkedOption.data('level') != "1") || (widget.optgroup && $checkedOption.data('level') != "2")) {
            parents.push($checkedOption.data('id'));
            selectedIds.push($checkedOption.data('id'));
            selectedNames.push(widget.trimText($checkedOption.text()));
            $checkedOption = widget.field.find('option:eq(' + $checkedOption.data('parent') + ')');
          }
          // Add parent element.
          parents.push($checkedOption.data('id'));
          selectedIds.push($checkedOption.data('id'));
          selectedNames.push(widget.trimText($checkedOption.text()));
          if (widget.optgroup) {
            $checkedOption = widget.field.find('optgroup').filterByData('id', $checkedOption.data('parent'));
            parents.push($checkedOption.data('id'));
            selectedNames.push($checkedOption.prop('label'));
          }
          // Add row to drop area.
          widget.addRowToDrop(selectedIds.reverse(), selectedNames.reverse());
          empty = false;
        }
      }
    });

    if (empty) {
      widget.elDropArea.find('tbody').append(widget.buildNothingRow());
    }

    // Build the widget.
    widget.elWidget = $('<div class="hs-lite-widget-wrapper"></div>');
    widget.elSelects = $('<div class="hs-lite-selects clearfix"></div>');
    widget.elButton = widget.buildAddButton();
    widget.elSelects.prepend(widget.elButton);

    widget.elWidget.append(widget.elSelects).append(widget.elDropArea);
    widget.field.after(widget.elWidget);

    // Add the select elements
    $level1Select = widget.addLevel(0, null);

    // Add option group elements, if any
    //widget.addOptionGroupLevel();

    // If there are children add another select.
    if (widget.field.find('option').filterByData('parent', 1).length) {
      $level2Select = widget.addLevel(1, $level1Select);
    }

    if (widget.optgroup && widget.field.find('option').filterByData('parent', 2).length) {
      widget.addLevel(2, $level2Select);
    }

  }

  /**
   * Figure out which level in the hierarchy the option passed is.
   *
   * @param $option The option element to figure out the level.
   * @returns numeric level.
   */
  Drupal.hs_lite.widget.prototype.getOptionLevel = function($option) {
    var widget = this;
    var level = '1';

    if (widget.optgroup) {
      level = '2';
      if ($option.prop("tagName") == "OPTGROUP") {
        return '1';
      }
    }
    var text = $option.text();
    if (text == '- None -') {
      return 0;
    }
    for (i = 0; i < text.length; i++) {
      if (text.substr(i, 1) == '-') {
        level++;
      }
      else {
        break;
      }
    }

    return level;
  }

  /**
   * Trims level hyphens from beginning of text.
   *
   * @param text The text to trim.
   * @returns Trimmed text.
   */
  Drupal.hs_lite.widget.prototype.trimText = function(text) {
    count = 0;
    for (i = 0; i < text.length; i++) {
      if (text.substr(i, 1) == '-') {
        count++;
      }
      else {
        break;
      }
    }
    return text.substr(count, text.length - count);
  }

  /**
   * Add another level to the widget. This means another select field.
   *
   * @param id - The numeric id of the option.
   * @param $parentSelect - The parent select element.
   * @returns Then new select element.
   */
  Drupal.hs_lite.widget.prototype.addLevel = function(id, $parentSelect) {
    var widget = this;

    // Get the children of the current id.
    var $items = widget.field.find('option, optgroup').filterByData('parent', id);

    // Only need to do this if we have children.
    if (!$items.length) {
      return null;
    }
    else {
      // If it's the first element then don't add a none option.
      var options = (id == 0 || (widget.optgroup && id == 1)) ? '' : '<option value="none"></option>';

      // Get all the options whose parent is the current id and build an array
      // of html options.
      $items.each(function() {
        var optionText = widget.trimText($(this).text());
        if (widget.optgroup && $(this).prop('tagName') == "OPTGROUP") {
          optionText = $(this).prop("label");
        }
        options += '<option value="' + $(this).data('id') + '">' + optionText + '</option>';
      });

      // Create an object of the select
      var $newSelect = $('<select class="hs-lite-select form-select">' + options + '</select>');

      // If we don't have a parent make the wrapper the parent. Then insert the
      // select into the dom.
      if ($parentSelect == null) {
        $newSelect.data('level', 1);
        widget.elSelects.prepend($newSelect);
      }
      else {
        $newSelect.data('level', $parentSelect.data('level') + 1);
        $parentSelect.after($newSelect);
      }

      // Add a change event so we can do some action on it.
      $newSelect.change(function() {
        widget.getNextLevel($(this));
      });

      // Return the new select that is created.
      return $newSelect;
    }
  }

  /**
   * Adds the next level when an element is changed.
   *
   * @param $select - The element being changed.
   */
  Drupal.hs_lite.widget.prototype.getNextLevel = function($select) {
    var widget = this;

    // Get some info about the changed select.
    var level = $select.data('level');
    var selection = $select.val();

    // Loop through all the selects that are of a higher level and remove
    // them.
    level++;
    while ($select.siblings('select').filterByData('level', level).length) {
      $select.siblings('select').filterByData('level', level).remove();
      level++;
    }

    // Go through the add Level process.
    widget.addLevel(selection, $select);
  }

  /**
   * Build the button.
   *
   * @returns The button element.
   */
  Drupal.hs_lite.widget.prototype.buildAddButton = function() {
    var widget = this;

    // Create an add button.
    var $button = $('<input type="button" value="Add" class="hs-lite-add-button form-submit">');

    // Add the click event for the button.
    $button.click(function() {
      widget.addButtonAction($(this));
    });

    return $button;
  }

  /**
   * On click event for when the add button is pressed.
   *
   * @param $button
   */
  Drupal.hs_lite.widget.prototype.addButtonAction = function($button) {
    var widget = this;
    var selectedNames = [];
    var selectedElements = [];
    var addsSelection = false;

    // Find the selections.
    widget.elSelects.find('select').each(function(index, value) {
      var selection = $(this).val();
      if (widget.optgroup && index == 0) {
        selectedElements.push($(this).val());
        selectedNames.push($(this).find('option:selected').text());
        return;
      }
      // Only act if there is a value.
      if (selection !== 'none') {
        selectedElements.push($(this).val());
        selectedNames.push($(this).find('option:selected').text());
        if (!widget.field.find('option').filterByData('id',selection).is(':selected')) {
          addsSelection = true;
          // Select the attribute in the field.
          widget.field.find('option').filterByData('id',selection).attr('selected', 'selected');
        }
      }
    });

    if (addsSelection) {
      widget.addRowToDrop(selectedElements, selectedNames);
    }
  }

  /**
   * Add a row to the drop area.
   *
   * @param selectedElements - array of elements that are selected for this row.
   * @param selectedNames - array of the names for the selections.
   */
  Drupal.hs_lite.widget.prototype.addRowToDrop = function(selectedElements, selectedNames) {
    var widget = this;

    // Create the settings if it doesn't exist.
    Drupal.settings.hs_lite[widget.id] = Drupal.settings.hs_lite[widget.id] || {};
    Drupal.settings.hs_lite[widget.id].selectedCounter = Drupal.settings.hs_lite[widget.id].selectedCounter || {};

    // Get the counter.
    var selectedCounter = Drupal.settings.hs_lite[widget.id].selectedCounter;

    // Add one in the counters for each element.
    var parentElementInfo = '';
    for (id in selectedElements) {
      selectedCounter[selectedElements[id]] = selectedCounter[selectedElements[id]] + 1 || 1;
      if (parentElementInfo.length == 0) {
        parentElementInfo = selectedElements[id];
      }
      else {
        parentElementInfo += '-' + selectedElements[id];
      }

      // If a row exists with the parent remove it.
      widget.elDropArea.find('.hs-lite-remove-link a').filterByData('element-info', parentElementInfo).parent().parent().remove();
    }

    // Save our counters back.
    Drupal.settings.hs_lite[widget.id].selectedCounter = selectedCounter;

    // Create a new row and add it to the other rows.
    var $row = widget.buildRow(selectedElements, selectedNames);
    widget.elDropArea.find('tbody').append($row);
    widget.elDropArea.find('tr.hs-lite-level-is-empty').remove();

    // Run through all the rows and reset the odd/even classes
    widget.resetTableRowClasses();
  }

  /**
   * Reset table row class
   */
  Drupal.hs_lite.widget.prototype.resetTableRowClasses = function () {
    var widget = this;

    var numRows = widget.elDropArea.find('tr');

    // Run through all the rows and reset the odd/even classes
    widget.elDropArea.find('tr').each(function(index, value){
      $(this).removeClass("odd");
      $(this).removeClass("even");
      $(this).removeClass("first");
      $(this).removeClass("last");
      // determine if the drop area is empty
      var emptyDropArea = false;
      if ($(this).attr("class") === "hs-lite-level-is-empty") {
        emptyDropArea = true;
      }
      // index is zero-based, so bump up by one
      index++;
      if (!emptyDropArea) {
        if (!(index%2)) {
          $(this).addClass("even");
        }
        else {
          $(this).addClass("odd");
        }
        if (index === 1) {
          $(this).addClass("first");
        }
        if (index === numRows.length) {
          $(this).addClass("last");
        }
      }
    });
  }

  /**
   * Builds a row for when there are no selections.
   *
   * @returns a row for no selections.
   */
  Drupal.hs_lite.widget.prototype.buildNothingRow = function () {
    var widget = this;

    var $row = $('<tr class="hs-lite-level-is-empty"><td>' + Drupal.t('Nothing has been selected.') + '</td></tr>');
    return $row;
  }

  /**
   * Build a row element
   *
   * @param selectedIds - array of the selected element id's
   * @param selectedNames - array of the selected element names.
   * @returns an jquery object row.
   */
  Drupal.hs_lite.widget.prototype.buildRow = function(selectedIds, selectedNames) {
    var widget = this;

    // Create row to be inserted.
    var $row = $('<tr class="droparea-entry"><td>' + selectedNames.join(' > ') + '</td><td class="hs-lite-remove-link"></td></tr>');
    // Create the remove link.
    var $removeLink = $('<a href="#" data-element-info="' + selectedIds.join('-') + '">' + Drupal.t('Remove') + '</a>');

    // Add to row
    $row.find('.hs-lite-remove-link').prepend($removeLink);

    // Provide action for clicking the remove link.
    $removeLink.click(function(event) {

      // Don't follow the link.
      event.preventDefault();

      widget.removeRow($(this));
    });

    return $row;
  }

  /**
   * Remove a row from the selections.
   *
   * @param $link
   */
  Drupal.hs_lite.widget.prototype.removeRow = function($link) {
    var widget = this;

    // Get the data attribute that determines what ids this row utilizes.
    var dashElements = $link.data('element-info');
    var selectedElements = (dashElements === parseInt(dashElements)) ? [dashElements] : dashElements.split('-');

    // Get the count of how many of which id has been used.
    var selectedCounter = Drupal.settings.hs_lite[widget.id].selectedCounter;

    // Go through the selected Elements
    for (id in selectedElements) {
      // Get the option id.
      var selectedId = parseInt(selectedElements[id]);

      // Lower by one since it is being removed.
      selectedCounter[selectedId]--;

      // If there are no more of these options being used then remove it from
      // the main select.
      if (selectedCounter[selectedId] == 0) {
        // Remove the selected attribute.
        widget.field.find('option').filterByData('id',selectedId).removeAttr('selected');
      }
    }

    // Delete the row.
    var $removeRow = $link.parent().parent();
    var $table = $removeRow.parent();
    $removeRow.remove();

    // If there are no more rows then add the nothing row.
    if (!$table.find('tr').length) {
      $table.prepend(widget.buildNothingRow());
    }

    // Run through all the rows and reset the odd/even classes
    widget.resetTableRowClasses();
  }

}(jQuery));
